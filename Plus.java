import com.cycling74.max.*;

public class Plus extends MaxObject
{

	private static final String[] INLET_ASSIST = new String[]{
		"inlet 1 help", "inlet 2 help"
	};
	private static final String[] OUTLET_ASSIST = new String[]{
		"outlet 1 help"
	};
	
	private Atom addend1 = Atom.newAtom(0);
	private Atom addend2 = Atom.newAtom(0);

	public Plus(Atom[] args)
	{
		declareIO(2,1);
		
		if (args.length > 0)
		{
			if (args[0].isInt() || args[0].isFloat())
			{
			addend2 = args[0];
			}	
		}

		setInletAssist(INLET_ASSIST);
		setOutletAssist(OUTLET_ASSIST);

	}
    
	public void inlet(int i)
	{
	if (getInlet()==0)
	{
		if (addend2.isInt())
		{
			addend1 = Atom.newAtom(i);
			outlet(0, addend1.getInt()+addend2.getInt());
		}	else {
						addend1 = Atom.newAtom((float)i);
						outlet(0,addend1.getFloat()+addend2.getFloat());
					}
	} else {
					if (addend2.isInt())
					{
						addend2 = Atom.newAtom(i);
						outlet(0,addend1.getInt()+addend2.getInt());
					} else {
									addend2 = Atom.newAtom((float)i);
									outlet(0,addend1.getFloat()+addend2.getFloat());
								}
				}
}

	public void inlet(float f)
	{
	if (getInlet()==0)
	{
		if (addend2.isInt())
		{
			addend1 = Atom.newAtom((int)f);
			outlet(0,addend1.getInt()+addend2.getInt());
		}	else {
						addend1 = Atom.newAtom(f);
						outlet(0,addend1.getFloat()+addend2.getFloat());
					}
	} else {
					if (addend2.isInt())
					{
						addend2 = Atom.newAtom((int)f);
						outlet(0,addend1.getInt()+addend2.getInt());
					} else {
									addend2 = Atom.newAtom(f);
									outlet(0,addend1.getFloat()+addend2.getFloat());
								}
				}    
		}
}



